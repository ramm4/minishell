#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lmatvien <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/07/06 20:11:35 by lmatvien          #+#    #+#              #
#    Updated: 2018/09/29 16:05:42 by lmatvien         ###   ########.fr        #
#                                                                              #
#******************************************************************************#
HEADER =	\n █░░ █▀▄▀█ █▀▀█ ▀▀█▀▀ ▀█░█▀ ░▀░ █▀▀ █▀▀▄ \n \
			█░░ █░▀░█ █▄▄█ ░░█░░ ░█▄█░ ▀█▀ █▀▀ █░░█ \n \
			▀▀▀ ▀░░░▀ ▀░░▀ ░░▀░░ ░░▀░░ ▀▀▀ ▀▀▀ ▀░░▀ \

#COLORSs
VIOLET      = \033[01;38;05;201m
RESET		= \033[m
RED         = \033[1;31m
CYAN        = \033[1;36m

NAME			= minishell
LIB_MINI_NAME 	= lib_mini.a
LIB_MINISHELL	= minishell.a
ODIR 			= obj
SDIR 			= srcs
BDIR			= ./srcs/build
LDIR 			= ./srcs/libft_mini
FT_PRINTF		= ./srcs/ft_printf/libftprintf.a

FLAGS  			= -Wall -Wextra -Werror
			
LIBFILE =   ft_atoi_base.c \
			ft_itoa.c \
			ft_strnew.c \
			ft_pow.c \
			ft_strchr.c \
			ft_dgt_quntt_i.c \
			ft_isdigit.c  \
			ft_strrchr.c \
			ft_putnbr_fd.c \
			ft_putstr_error.c \
			ft_str_del_newline.c \
			ft_putchar.c \
			ft_putstr.c \
			ft_strcpy.c \
			ft_strncpy.c \
			ft_strcmp.c \
			ft_strequ.c \
			ft_strlen.c \
			ft_strnlen.c \
	 		ft_strdup.c \
			ft_strjoin.c \
			ft_strstr.c \
			ft_memset.c \
			ft_memcpy.c \
			ft_strncat.c \
			ft_strcat.c \
			get_next_line.c \
			ft_strsplit.c \
			ft_atoi.c \
			ft_swap.c \
			ft_to_uppercase.c \
			ft_to_lowercase.c \

SHELL_FILLES =	shell_main.c \

LIB_OBJ =	$(addprefix $(ODIR)/, $(patsubst %.c,%.o,$(LIBFILE)))
SHELL_OBJ =	$(addprefix $(ODIR)/, $(patsubst %.c,%.o,$(SHELL_FILLES)))

INC_PARSER = -I $(LDIR) -I srcs/ft_printf/includes/

all: $(NAME)

$(ODIR)/%.o: $(LDIR)/%.c | $(ODIR)
	@gcc -c $< -o $@
	@echo "$(VIOLET)█$(RESET)\c)"

$(ODIR)/%.o: $(BDIR)/%.c | $(ODIR)
	@gcc -c $(INC_PARSER) $< -o $@
	@echo "$(VIOLET)█$(RESET)\c)"

$(ODIR):
	@echo "\033[35;1m>>>Creating objects folder...\033[0m"
		@echo "$(CYAN)Linking files ... $(RESET)\c)"
	@mkdir $(ODIR)

$(NAME): $(LIB_OBJ) $(SHELL_OBJ)
	@ar rcs ./obj/$(LIB_MINI_NAME) $(LIB_OBJ)
	@make -C ./srcs/ft_printf/
	@echo "$(CYAN)█$(RESET)\c)"
	@echo "\033[0;31m\nDone$(HEADER)\033[0m";
clean:
	@/bin/rm -rf $(LIB_OBJ)
	@make clean -C ./srcs/ft_printf/
	@echo "\033[35;1m>>>Removed OBJ Files\033[0m"
	
fclean: clean
	@/bin/rm -rf $(ODIR) $(NAME)
	@make fclean -C ./srcs/ft_printf/
	@echo "\033[35;1m>>>Removed executed files\033[0m"

re: fclean all
