/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shell_environment.h                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lmatvien <lmatvien@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/17 13:15:38 by lmatvien          #+#    #+#             */
/*   Updated: 2019/01/17 13:15:39 by lmatvien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SHELL_ENVIRONMENT_H
# define SHELL_ENVIRONMENT_H

#include "libft.h"

void	shell_loop(void);
void	prompt_with_time(void);
#endif