/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shell_prompt_type.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lmatvien <lmatvien@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/17 13:59:46 by lmatvien          #+#    #+#             */
/*   Updated: 2019/01/17 14:00:26 by lmatvien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <time.h>
#include "libft.h"
#include <stdio.h>

static char *ullto00_format(int value)
{
	char *data;
	char *str;
	int indent;

	if (value < 10)
	{
		if (!(data = ft_strnew(0x2)))
			return (NULL);
		ft_memset(data, 0x30, 0x2);
		indent = 0x2 - ft_dgt_quntt_i(value);
		if (!(str = ft_itoa(value)))
			return (NULL);
		ft_memcpy(data + indent, str, ft_strlen(str));
		free(str);
	}
	else
		data = ft_itoa(value);
	return (data);
}

void	prompt_with_time(void)
{
	time_t		rawtime;
	struct tm	*timeinfo;
	char		*temp;
	char		*temp2;
	char		*curr_time;
	
	time(&rawtime);
	timeinfo = localtime (&rawtime);
	temp = ullto00_format(timeinfo->tm_mday);
	temp2 = ft_strjoin(temp, "/");
	free(temp);
	temp = ullto00_format(timeinfo->tm_mon + 1);
	curr_time = ft_strjoin(temp2, temp);
	free(temp);
	free(temp2);
	temp = ft_strjoin("");

	curr_time = ft_strjoin(temp, temp2);
	free(temp);
	free(temp2);
	temp = ft_itoa((timeinfo->tm_year + 1900) - 2000);
	//	printf("%d/%s/%d", timeinfo->tm_mday, ullto00_format(timeinfo->tm_mon + 1), (timeinfo->tm_year + 1900) - 2000);
}