/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shell_main.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lmatvien <lmatvien@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/15 11:57:06 by lmatvien          #+#    #+#             */
/*   Updated: 2019/01/15 11:57:07 by lmatvien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell_environment.h"
#include <stdio.h>

extern char **environ;

int	main(void)
{
	size_t	il;

	il = 0;

	shell_loop();

	return 0;
}
