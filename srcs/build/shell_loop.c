/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shell_loop.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lmatvien <lmatvien@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/17 13:16:05 by lmatvien          #+#    #+#             */
/*   Updated: 2019/01/17 13:16:06 by lmatvien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell_environment.h"
#include <stdio.h>

#define STDIN_	0

void	shell_loop(void)
{
	int		sh_status;
	char	*cmmnd_line;

	sh_status = 0;
	write(1, "$> ", 3);
	while ((sh_status = get_next_line(STDIN_, &cmmnd_line)) > 0)
	{
		write(1, "$> ", 3);
	}
	prompt_with_time();
}